
# Product Calculator Lambda Function

This project contains a Rust-based AWS Lambda function that calculates the product of two numbers. It leverages the `lambda_http` crate to handle HTTP requests within the AWS Lambda environment, providing a simple and efficient way to perform calculations in the cloud.

## Getting Started

### Prerequisites

- Rust and Cargo Lambda installed on your machine.
- AWS CLI configured with appropriate access rights.
- Basic knowledge of AWS Lambda and Rust.

### Installation

Clone this repository to your local machine using:

```sh
git clone https://gitlab.com/HiepDuke/mini_Project2
cd miniProject2
```

Build the project with Cargo:

```sh
cargo build --release --arm64
```

### Deployment



Deploy your Lambda function using the AWS CLI:

```sh
cargo lambda deploy --iam-role [role]
```

Ensure to replace [role] with your AWS credentials, if you want to use a predefined IAM role..

### Usage

Invoke your Lambda function with the AWS CLI, providing two numbers as query parameters:

```sh
cargo lambda invoke --remote miniProject2 --data-ascii "{\"x\":-1, \"y\":20 }"
```

The function will return the product of the two numbers. {"product":-20} is the response from this Lambda function with this instance.

## Contributing

Contributions are what make the open-source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

